// Escreva uma função que converta a data de entrada do usuário formatada como MM/DD/YYYY em um formato exigido por uma API (YYYYMMDD). O parâmetro "userDate" e o valor de retorno são strings.

// Por exemplo, ele deve converter a data de entrada do usuário "31/12/2014" em "20141231" adequada para a API.


function formatDate(userDate) {
  // format from M/D/YYYY to YYYYMMDD
  var newDate = '';

  // Formata usando as substrings
  newDate = userDate.substring(6,10)  + userDate.substring(0,2) + userDate.substring(3,5);

  return newDate;
}

console.log(formatDate("12/31/2014"));

// Outros testes
console.log(formatDate("02/04/1998"));
console.log(formatDate("23/02/1997"));