// Implemente a função removeProperty, que recebe um objeto e o nome de uma propriedade.

// Faça o seguinte:

// Se o objeto obj tiver uma propriedade prop, a função removerá a propriedade do objeto e retornará true;
// em todos os outros casos, retorna falso.

function removeProperty(obj, prop) {

	// Verifica se existe esta propriedade
	// se nao existir, retorna false
	if(typeof(obj[prop]) === "undefined"){
		return false
		}
	// se existir deleta a propriedade e retorna
	// true
	else{
		return delete obj[prop];
	}
}

var pet = {
  name:"Steve",
  parent:"Tiago",
  species:"hamster",
  ageInMoths: 1,
  color:"brown"
};


// Test
console.log(removeProperty(pet, "name"));