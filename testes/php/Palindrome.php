<?php

/* ----------------- DESCRIÇÃO DO TESTE -----------------------*/

/*
Um palíndromo é uma palavra que pode ser lida  da mesma maneira da esquerda para direita, como ao contrário.
Exemplo: ASA.

Faça uma função que ao receber um input de uma palavra, string, verifica se é um palíndromo retornando verdadeiro ou falso.
O fato de um caracter ser maiúsculo ou mínusculo não deverá influenciar no resultado da função.

Exemplo: isPalindrome("Asa") deve retonar true.
*/




class Palindrome
{
    public static function isPalindrome($word)
    {
        // Converte todos os caracteres para uppercase
    	$upperCaseWord = strtoupper($word);

        // Obtem a palavra invertida usando strrev
    	$revWord = strrev($upperCaseWord);

        // Compara a palavra original com sua versao
        // invertida
    	if($revWord == $upperCaseWord){
    		return true;
    	}
    	else{
    		return false;
    	}
    }
}

echo Palindrome::isPalindrome('Deleveled') ? "true\n" : "false\n";

//Outros testes
echo Palindrome::isPalindrome('Mask') ? "true\n" : "false\n";