<?php


/* ----------------- DESCRIÇÃO DO TESTE -----------------------*/

/*
Implemente uma função que ao receber um array associativo contendo arquivos e donos, retorne os arquivos agrupados por dono. 

Por exempolo, um array ["Input.txt" => "Jose", "Code.py" => "Joao", "Output.txt" => "Jose"] a função groupByOwners deveria retornar ["Jose" => ["Input.txt", "Output.txt"], "Joao" => ["Code.py"]].


*/

class FileOwners
{
    public static function groupByOwners($files)
    {
        // Converte todos para lowercase para ignorar diferencas
        // entre letra maiuscula e minuscula, considerando, por exemplo,
        // que 'maria' e 'Maria' sao a mesma pessoa
    	$filesLowerCase = array_map('strtolower', $files);

        // Inicializa o array que ira conter os nomes dos arquivos de cada
        // dono
    	$filesOwners = array();

        // Preenche o array com os nomes das pessoas
    	foreach ($filesLowerCase as $key => $value) {
    		$filesOwners[$value] = array_keys($filesLowerCase, $value);
		}

        // Contador de iteracoes apenas para formatar o print do resultado
		$itCounter1 = 0;

        echo "[";
        
        // Percorre o array criado por nos
    	foreach ($filesOwners as $key => $value) {

            // Pega o nome do dono de arquivos e converte a primeira
            // letra pra uppercase, para formatacao de print
    		$name = ucfirst($key);
    		echo "$name => [";

            // Tambem para formatar o print
    		$itCounter2 = 0;

            // Exibe o nome dos arquivos do proprietario
    		foreach ($value as $file) {

                // Controla a insercao do ';' ou nao com o contador
                // de iteracoes, nao colocando ';' depois do ultimo
                // arquivo
    			if( $itCounter2 != count( $value ) - 1) { 
			        echo "$file; ";
			    } 
			    else{
    		    	echo "$file";
			    }

			    $itCounter2++;
    		}

            // Controla a insercao do segundo ']' e da quebra de linha
            // ou nao com o contador de iteracoes
    		if( $itCounter1 != count( $filesOwners ) - 1) { 
			    echo "], ";
			} 
			else{
	    	  	echo "]]\n";
			}

			$itCounter1++;
		}

        // Retorna o array feito nesta funcao, caso o programador desejar
        // utiliza-lo futuramente
        return $filesOwners;
    }
}

$files = array
(
    "Input.txt" => "Jose",
    "Code.py" => "Joao",
    "Output.txt" => "Jose",
    "Click.js" => "Maria",
    "Out.php" => "maria",

);

//Teste do print
FileOwners::groupByOwners($files);

// Comentei apenas para ficar na formatacao de exemplo, mais visual
//var_dump(FileOwners::groupByOwners($files));